import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PedidoTabPage } from './pedido-tab.page';

const routes: Routes = [
  {
    path: '',
    component: PedidoTabPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PedidoTabPageRoutingModule {}
