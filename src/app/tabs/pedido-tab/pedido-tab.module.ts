import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PedidoTabPageRoutingModule } from './pedido-tab-routing.module';

import { PedidoTabPage } from './pedido-tab.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PedidoTabPageRoutingModule
  ],
  declarations: [PedidoTabPage]
})
export class PedidoTabPageModule {}
