import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PedidoTabPage } from './pedido-tab.page';

describe('PedidoTabPage', () => {
  let component: PedidoTabPage;
  let fixture: ComponentFixture<PedidoTabPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PedidoTabPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PedidoTabPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
