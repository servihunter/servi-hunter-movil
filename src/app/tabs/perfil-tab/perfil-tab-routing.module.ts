import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PerfilTabPage } from './perfil-tab.page';

const routes: Routes = [
  {
    path: '',
    component: PerfilTabPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PerfilTabPageRoutingModule {}
