import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PerfilTabPage } from './perfil-tab.page';

describe('PerfilTabPage', () => {
  let component: PerfilTabPage;
  let fixture: ComponentFixture<PerfilTabPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerfilTabPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PerfilTabPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
