import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PerfilTabPageRoutingModule } from './perfil-tab-routing.module';

import { PerfilTabPage } from './perfil-tab.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PerfilTabPageRoutingModule
  ],
  declarations: [PerfilTabPage]
})
export class PerfilTabPageModule {}
